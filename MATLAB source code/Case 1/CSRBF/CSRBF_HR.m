clear;
clc;
%%Case 1 control of synchronization of neuron population with
%%Hindmarch-Rose (HR) PRC
%% Method : CSRBF
N=200;
M=200;
omega=2*pi/0.2;
T=0.2;
dt=T/M;
S=10*M;
TT=linspace(0,10*T,S+1);
K=5000;
u_max=6;
count=0;
b=26;
mu=pi;
population=100;
domain=linspace(0,2*pi,N);
Z=(1-cos(domain))/(2*pi);   %HR PRC
Zp=sin(domain)/(2*pi);
% LSD =0.5; % or 0.1
% difference=1E-07;
% [X]=ndgrid(domain);
% P=DistanceMatrix(domain',domain',difference);
% zr=zeros(N,1);
% [RBF,DRBF,~,DDRBF]=Wendland(LSD,difference,'C6',P,X,zr);
load('RBFmatrix.mat'); %%Prefabricated RBF matrices
%%%%%%%%%%%%%%%%%%%
phi=zeros(S+1,N);
phif=zeros(S+1,N);
dphi=zeros(S+1,N);
ddphi=zeros(S+1,N);
dphif=zeros(S+1,N);
phi(1,:)=(exp(b*cos(domain-pi))/(2*pi*besseli(0,b)));
phif(1,:)=1/(2*pi);
dphi(1,:)=(-b*sin(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b));
ddphi(1,:)=(-b*cos(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b))+((b^2*sin(domain-pi).^2.*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b)));
dphif(1,:)=0;
u=zeros(S+1,1);
L2=zeros(S+1,1);
L2(1)=trapz(domain,(phi(1,:)-phif(1,:)).^2);
tic
th=linspace(0,2*pi,1000);
thetas=zeros(length(S),population);
thetas(1,:)=randraw('vonmises', [pi, b], population ); 
STB=((0.3^2/(2*pi))*trapz(domain,(Z.^2)))/2;
for i=1:S
I=(trapz(domain,(phi(i,:)-phif(1,:)).*Zp.*phi(i,:)));
    A1=RBF+dt*(omega*DRBF+u(i)*diag(Zp)*RBF+u(i)*diag(Z)*DRBF-STB*DDRBF);
    B1=phi(i,:);
    A2=RBF(1,:)-RBF(N,:);
    B2=0;
    A=[A1(2:N,:);A2];
    B=[B1(2:N),B2];
    weight=(A\B');
    phi(i+1,:)=RBF*weight;
    dphi(i+1,:)=DRBF*weight;
    ddphi(i+1,:)=DDRBF*weight;
    dphif(i+1,:)=dphif(i,:);
    L2(i+1)=trapz(domain,(phi(i+1,:)-phif(1,:)).^2);
    u(i+1)=min(abs(K*I),u_max)*I/abs(I);
    thetas(i+1,:)=RK4(thetas(i,:),u(i+1),T,dt);  %The effect of control parameter on 100 uncoupled neurons
end
    TIME=toc
    figure(1)
    plot(TT,u)
    xlabel('t')
    ylabel('u(t)')
    figure(2)
    plot(domain,phi(1,:))
    hold on 
    plot(domain,phi(51,:))
    plot(domain,phi(501,:))
    plot(domain,phi(1001,:))
    plot(domain,phi(2001,:))
    plot(domain,phif(1,:))
    hold off
    ylim([0 2]);
    xlim([0 2*pi])
    xlabel('\theta')
    ylabel('\rho')
    figure(3)
    set(gca, 'YScale', 'log')
    semilogy(TT,L2)
    xlabel('t')
    ylabel('L_2 Error')
     figure(4)
    clf
    plot(cos(thetas(1,:)),sin(thetas(1,:)),'m h',cos(th),sin(th))
    hold on
    plot(cos(thetas(end,:)),sin(thetas(end,:)),'b o',cos(th),sin(th))