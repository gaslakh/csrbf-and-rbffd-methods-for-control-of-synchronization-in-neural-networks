clc
clear;
%%Case 1 control of synchronization of neuron population with
%%Hindmarch-Rose (HR) PRC
%% Method : RBFFD
N=200;
omega=2*pi/0.2;
T=0.2;
M=200;
S=10*M;
TT=linspace(0,10*T,S+1);
c=80;
dt=T/M;
BB=6;
K=5000;
count=0;
b=26;
mu=pi;
domain=linspace(0,2*pi,N);
[X,Y]=meshgrid(domain,domain);
Z=(1-cos(domain))/(2*pi);       %%HR PRC
Zp=sin(domain)/(2*pi);
phi=zeros(S+1,N);
phif=zeros(S+1,N);
dphi=zeros(S+1,N);
dphif=zeros(S+1,N);
phi(1,:)=(exp(b*cos(domain-pi))/(2*pi*besseli(0,b)));
phif(1,:)=1/(2*pi);
dphi(1,:)=(-b*sin(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b));
dphif(1,:)=0;
population=100;
load('RBFFDmatrix.mat') %%Prefabricated RBF-FD matrices
th=linspace(0,2*pi,1000);
thetas=zeros(length(S),population);
 thetas(1,:)=randraw('vonmises', [pi, b], population ); %
u=zeros(S+1,1);
L2=zeros(S+1,1);
L2(1)=trapz(domain,(phi(1,:)-phif(1,:)).^2);
tic;
for i=1:S
    I=(trapz(domain,(phi(i,:)-phif(1,:)).*Zp.*phi(i,:)));
    A1=w+dt*(omega*wd+u(i)*diag(Zp)*w+u(i)*diag(Z)*wd);
    B1=phi(i,:);
    A2=w(1,:)-w(N,:);
    B2=0;
    A=[A1;A2];
    B=[B1,B2];
    phi(i+1,:)=A\B';
	dphif(i+1,:)=dphif(i,:);
    L2(i+1)=trapz(domain,(phi(i+1,:)-phif(1,:)).^2);
    dphi(i+1,:)=(wd*phi(i+1,:)');
	u(i+1)=min(abs(K*I),BB)*I/abs(I);
    thetas(i+1,:)=RK4(thetas(i,:),u(i+1),T,dt);  %The effect of control parameter on 100 uncoupled neurons
end
    TIME=toc
    figure(1)
    plot(TT,u)
    xlabel('\tau')
    ylabel('u(\tau)')
    figure(2)
    plot(domain,phi(1,:))
    hold on 
    plot(domain,phi(51,:))
    plot(domain,phi(501,:))
    plot(domain,phi(1001,:))
    plot(domain,phi(2001,:))
    plot(domain,phif(1,:))
    hold off
    ylim([0 2]);
    xlim([0 2*pi])
    xlabel('\theta')
    ylabel('\rho')
    figure(3)
    set(gca, 'YScale', 'log')
    semilogy(TT,L2)
    xlabel('\tau')
    ylabel('Error')
    figure(4)
    clf
    plot(cos(thetas(1,:)),sin(thetas(1,:)),'m h',cos(th),sin(th))
    hold on
    plot(cos(thetas(end,:)),sin(thetas(end,:)),'r o',cos(th),sin(th))
