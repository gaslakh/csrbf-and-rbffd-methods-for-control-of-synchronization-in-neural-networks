clear;
clc;
%%Case 2 control of synchronization of neuron population with
%%Hudgkin-Huxley (HH) PRC
%% Method : CSRBF
N=150;
M=200;
omega=2*pi/14.638;
T=14.638;
dt=T/M;
S=5*M;
K=600;
BB=1.7;
count=0;
b=26;
mu=pi;
TT=linspace(0,5*T,S+1);
domain=linspace(0,2*pi,N);
load('PRCs.mat')    %HH PRC
% LSD =0.4; % or 0.1
% difference=1E-07;
% % domain=dom';
% [X]=ndgrid(domain);
% P=DistanceMatrix(domain',domain',difference);
% zr=zeros(N,1);
% [RBF,DRBF]=Wendland(LSD,difference,'C6',P,X,zr);
load('RBFmatrix.mat') %%Prefabricated RBF matrices
%%%%%%%%%%%%%%%%%%%
phi=zeros(S+1,N);
phif=zeros(S+1,N);
dphi=zeros(S+1,N);
dphif=zeros(S+1,N);
phi(1,:)=(exp(b*cos(domain-pi))/(2*pi*besseli(0,b)));
population=100;
phif(1,:)=1/(2*pi);
dphi(1,:)=(-b*sin(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b));
dphif(1,:)=0;
u=zeros(S+1,1);
L2=zeros(S+1,1);
L2(1)=trapz(domain,(phi(1,:)-phif(1,:)).^2);
th=linspace(0,2*pi,1000);
thetas=zeros(length(S),population);
thetas(1,:)=randraw('vonmises', [pi, b], population ); 
tic
for i=1:S
    I=(trapz(domain,(phi(i,:)-phif(1,:)).*Zp'.*phi(i,:)));
    A1=RBF+dt*(omega*DRBF+u(i)*diag(Zp')*RBF+u(i)*diag(Z')*DRBF);
    B1=phi(i,:);
    A2=RBF(1,:)-RBF(N,:);
    B2=0;
    A=[A1(2:N,:);A2];
    B=[B1(2:N),B2];
    weight=A\B';
    phi(i+1,:)=RBF*weight;
    dphi(i+1,:)=DRBF*weight;
    dphif(i+1,:)=dphif(i,:);
    L2(i+1)=trapz(domain,(phi(i+1,:)-phif(1,:)).^2);
    u(i+1)=min(abs(K*I),BB)*I/abs(I);
    thetas(i+1,:)=RK4HH(thetas(i,:),u(i+1),T,dt); %The effect of control parameter on 100 uncoupled neurons
end
    TIME=toc
    figure(1)
    plot(TT,u)
    xlabel('\tau')
    ylabel('u(\tau)')
    figure(2)
    plot(domain,phi(1,:))
    hold on 
    plot(domain,phi(101,:))
    plot(domain,phi(201,:))
    plot(domain,phi(501,:))
    plot(domain,phi(1001,:))
    plot(domain,phif(1,:))
    hold off
    ylim([0 2]);
    xlim([0 2*pi])
    xlabel('\theta')
    ylabel('\rho')
    figure(3)
    set(gca, 'YScale', 'log')
    semilogy(TT,L2)
    xlim([0 4*14.7])
    xlabel('\tau')
    ylabel('Error')
    figure(4)
    clf
    plot(cos(thetas(1,:)),sin(thetas(1,:)),'m h',cos(th),sin(th))
    hold on
    plot(cos(thetas(end,:)),sin(thetas(end,:)),'b o',cos(th),sin(th))
