function L=RK4HH(tts,u,T,dt);
step=3;
h=dt/step;
omega=2*pi/T;
f=@(TTS,U) omega+U*(PRCHH(TTS));
y=zeros(step+1,size(tts,2));
y(1,:)=tts;
for i=1:step
    kth1=f(y(i,:),u)';
    th1=y(i,:)+kth1*h*0.5;
    kth2=f(th1,u)';
    th2=y(i,:)+kth2*h*0.5;
    kth3=f(th2,u)';
    th3=y(i,:)+kth3*h;
    kth4=f(th3,u)';
    y(i+1,:)=y(i,:)+h*(kth1+2*kth2+2*kth3+kth4)/6;
end
L=y(i+1,:);
end
