clear;
clc;
%%Case 5 control of synchronization of neuron population with
%%Kuramoto  PRC
%% Method : CSRBF
N=150;
M=150;
omega=2*pi;
T=1;
dt=T/M;
S=10*M;
u_max=4;
K=250;
count=0;
b=26;
mu=pi;
TT=linspace(0,10*T,S+1);
population=100;
domain=linspace(0,2*pi,N);
Z=sin(domain');      %%Kuramoto PRC
Zp=cos(domain');
% LSD =0.4; % or 0.1
% difference=1E-07;
% [X]=ndgrid(domain);
% P=DistanceMatrix(domain',domain',difference);
% zr=zeros(N,1);
% [RBF,DRBF]=Wendland(LSD,difference,'C2',P,X,zr);
load('RBFmatrices.mat');   %%Prefabricated RBF matrices
phi(1,:)=(exp(b*cos(domain-pi))/(2*pi*besseli(0,b)));
temp1=sum(phi(1,:));
temp2=phi(1,:)/temp1;
temp3=zeros(N,1);
temp3(1)=temp2(1);
phif(1,:)=1/(2*pi);
dphi(1,:)=(-b*sin(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b));
dphif(1,:)=0;
u=zeros(S+1,1);
L2=zeros(S+1,1);
L2(1)=trapz(domain,(phi(1,:)-phif(1,:)).^2);
th=linspace(0,2*pi,1000);
thetas=zeros(length(S),population);
thetas(1,:)=randraw('vonmises', [pi, b], population ); 
tic
for i=1:S
    I=(trapz(domain,(phi(i,:)-phif(1,:)).*Zp'.*phi(i,:)));
    A=RBF+dt*(omega*DRBF+u(i)*diag(Zp')*RBF+u(i)*diag(Z')*DRBF);
    B=phi(i,:);
    A2=RBF(1,:)-RBF(N,:);
    B2=0;
    A(1,:)=A2;
    B(1)=B2;
    weight=A\B';
    phi(i+1,:)=RBF*weight;
    dphi(i+1,:)=DRBF*weight;
    dphif(i+1,:)=dphif(i,:);
    L2(i+1)=trapz(domain,(phi(i+1,:)-phif(1,:)).^2);
    u(i+1)=min(abs(K*I),u_max)*I/abs(I);   
    thetas(i+1,:)=RK4Ku(thetas(i,:),-u(i+1),T,dt);      %The effect of control parameter on 100 uncoupled neurons
end
    TIME=toc
    figure(1)
    plot(TT,u)
    xlabel('\tau')
    ylabel('u(\tau)')
    figure(2)
    plot(domain,phi(1,:))
    hold on 
    plot(domain,phi(39,:))
    plot(domain,phi(384,:))
    plot(domain,phi(751,:))
    plot(domain,phi(1501,:))
    plot(domain,phif(1,:))
    hold off
    ylim([0 2]);
    xlim([0 2*pi])
    xlabel('\theta')
    ylabel('\rho')
    figure(3)
    set(gca, 'YScale', 'log')
    semilogy(TT,L2)
    xlabel('\tau')
    ylabel('Error')
    figure(4)
    clf
    plot(cos(thetas(1,:)),sin(thetas(1,:)),'k *',cos(th),sin(th))
    hold on
    plot(cos(thetas(end,:)),sin(thetas(end,:)),'b o',cos(th),sin(th))
