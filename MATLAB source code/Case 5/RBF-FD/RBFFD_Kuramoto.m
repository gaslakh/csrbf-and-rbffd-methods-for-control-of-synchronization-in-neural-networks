clc
clear;
%%Case 5 control of synchronization of neuron population with
%%Kuramoto PRC
%% Method : RBFFD
N=150;
omega=2*pi/1;
T=1;
M=150;
S=10*M;
c=80;
dt=T/M;
u_max=4;
K=250;
count=0;
b=26;
mu=pi;
TT=linspace(0,10*T,S+1);
population=100;
domain=linspace(0,2*pi,N);
[X,Y]=meshgrid(domain,domain);
Z=-sin(domain);         %%Kuramoto PRC
Zp=-cos(domain);
%%%%%%%%%%%%%%%%%%PRI%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
phi=zeros(S+1,N);
phif=zeros(S+1,N);
dphi=zeros(S+1,N);
dphif=zeros(S+1,N);
phi(1,:)=(exp(b*cos(domain-pi))/(2*pi*besseli(0,b)));
phif(1,:)=1/(2*pi);
dphi(1,:)=(-b*sin(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b));
dphif(1,:)=0;
load('RBFFDmatrices.mat') %%Prefabricated RBFFD matrices
u=zeros(S+1,1);
L2=zeros(S+1,1);
L2(1)=trapz(domain,(phi(1,:)-phif(1,:)).^2);
th=linspace(0,2*pi,1000);
thetas=zeros(length(S),population);
thetas(1,:)=randraw('vonmises', [pi, b], population ); 
tic;
for i=1:S
I=(trapz(domain,(phi(i,:)-phif(1,:)).*Zp.*phi(i,:)));
    A1=w+dt*(omega*wd+u(i)*diag(Zp)*w+u(i)*diag(Z)*wd);
    B1=phi(i,:);
    A2=w(1,:)-w(N,:);
    B2=0;
    A=[A1;A2];
    B=[B1,B2];
    phi(i+1,:)=A\B';
	dphif(i+1,:)=dphif(i,:);
    L2(i+1)=trapz(domain,(phi(i+1,:)-phif(1,:)).^2);
    dphi(i+1,:)=(wd*phi(i+1,:)');
    u(i+1)=min(abs(K*I),u_max)*I/abs(I);  
     thetas(i+1,:)=RK4Ku(thetas(i,:),u(i+1),T,dt);     %The effect of control parameter on 100 uncoupled neurons
end
    TIME=toc
    TT=linspace(0,T,S+1);
    trapz(TT,(u).^2)
    figure(1)
    plot(TT,u)
    xlabel('\tau')
    ylabel('u(\tau)')
    figure(2)
    plot(domain,phi(1,:))
    hold on 
    plot(domain,phi(39,:))
    plot(domain,phi(384,:))
    plot(domain,phi(751,:))
    plot(domain,phi(1501,:))
    plot(domain,phif(1,:))
    hold off
    ylim([0 2]);
    xlim([0 2*pi])
    xlabel('\theta')
    ylabel('\rho')
    figure(3)
    set(gca, 'YScale', 'log')
    semilogy(TT,L2)
    xlabel('\tau')
    ylabel('Error')
    figure(4)
    clf
    plot(cos(thetas(1,:)),sin(thetas(1,:)),'k *',cos(th),sin(th))
    hold on
    plot(cos(thetas(end,:)),sin(thetas(end,:)),'r o',cos(th),sin(th))
