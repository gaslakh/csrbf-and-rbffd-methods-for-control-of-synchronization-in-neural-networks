function L=RK4StockHH(tts,u,T,dt,D);
step=3;
h=dt/step;
omega=2*pi/T;
f=@(TTS,U) omega+U*(PRCHH(TTS));
g=@(TTS,D) sqrt(2*D)*(PRCHH(TTS));
y=zeros(step+1,size(tts,2));
y(1,:)=tts;
for i=1:step
    w=normrnd(0,h);
    kth1=f(y(i,:),u)';
    skth1=g(y(i,:),D)';
    th1=y(i,:)+kth1*h*0.5+skth1*w*0.5;
	kth2=f(th1,u)';
	skth2=g(th1,D)';
    th2=y(i,:)+kth2*h*0.5+skth2*w*0.5;
    kth3=f(th2,u)';
    skth3=g(th2,D)';
	th3=y(i,:)+kth3*h+skth3*w;
    kth4=f(th3,u)';
	skth4=g(th3,D)';
	%%%%%%
    y(i+1,:)=y(i,:)+1/6*(h*(kth1+2*kth2+2*kth3+kth4)+(w*(skth1+2*skth2+2*skth3+skth4)));
end
L=y(i+1,:);
end
