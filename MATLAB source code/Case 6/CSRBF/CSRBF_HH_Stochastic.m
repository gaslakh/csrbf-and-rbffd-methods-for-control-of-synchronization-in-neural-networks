clear;
clc;
%%Case 6 control of synchronization of neuron population with
%%Hudgkin-Huxley (HH) PRC (stochastic)
%% Method : CSRBF
N=200;
M=200;
omega=2*pi/14.638;
T=14.638;
dt=T/M;
S=5*M;
population=100;
TT=linspace(0,5*T,S+1);
u_max=8;
K=400;
count=0;
b=26;
mu=pi;
domain=linspace(0,2*pi,N);
load('PRCs.mat')       %%HH PRC
% LSD =0.3; % or 0.1
% difference=1E-07;
% [X]=ndgrid(domain);
% P=DistanceMatrix(domain',domain',difference);
% zr=zeros(N,1);
% [RBF,DRBF,~,DDRBF]=Wendland(LSD,difference,'C2',P,X,zr);
load('RBFmatrices.mat')   %%Prefabricated RBF matrices
phi(1,:)=(exp(b*cos(domain-pi))/(2*pi*besseli(0,b)));
phif(1,:)=1/(2*pi)*ones(1,N);
dphi(1,:)=(-b*sin(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b));
ddphi(1,:)=(-b*cos(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b))+((b^2*sin(domain-pi).^2.*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b)));
dphif(1,:)=0;
population=100;
temp1=sum(phi(1,:));
temp2=phi(1,:)/temp1;
temp3=zeros(N,1);
temp3(1)=temp2(1);
u=zeros(S+1,1);
L2=zeros(S+1,1);
L2(1)=trapz(domain,(phi(1,:)-phif(1,:)).^2);
th=linspace(0,2*pi,1000);
thetas=zeros(length(S),population);
thetas(1,:)=randraw('vonmises', [pi, b], population ); 
tic
D= 0.0040;%0.00045 0.0018 0.0040
sqrt(2*D)
STB=((2*D/(2*pi))*trapz(domain,(Z.^2)))/2
for i=1:S
	I=(trapz(domain,(phi(i,:)-phif(1,:)).*Zp.*phi(i,:)));
	G=-STB*(trapz(domain,(dphi(i,:)-dphif(1,:)).*dphi(i,:)));
    A1=RBF+dt*(omega*DRBF+u(i)*diag(Zp)*RBF+u(i)*diag(Z)*DRBF-STB*DDRBF);
    B1=phi(i,:);
    A2=RBF(1,:)-RBF(N,:);
    B2=0;
    A3=DRBF(1,:)-DRBF(N,:);
    B3=0;
    A=[A1;A2;A3];
    B=[B1,B2,B3];
    weight=(A\B');
    phi(i+1,:)=RBF*weight;
    dphi(i+1,:)=DRBF*weight;
    ddphi(i+1,:)=DDRBF*weight;
    dphif(i+1,:)=dphif(i,:);
    L2(i+1)=trapz(domain,(phi(i+1,:)-phif(1,:)).^2);
    u(i+1)=min(abs(-K*I-G./I),u_max)*I/abs(I);   
    thetas(i+1,:)=RK4StockHH(thetas(i,:),u(i+1),T,dt,D);         %The effect of control parameter on 100 uncoupled neurons
end
    TIME=toc
    figure(1)
    plot(TT,u)
    xlabel('\tau')
    ylabel('u(\tau)')
    figure(2)
    plot(domain,phi(1,:))
    hold on 
    plot(domain,phi(101,:))
    plot(domain,phi(201,:))
    plot(domain,phi(501,:))
    plot(domain,phi(1001,:))
    plot(domain,phif(1,:))
    hold off
    ylim([0 2]);
    xlim([0 2*pi])
    xlabel('\theta')
    ylabel('\rho')
    figure(3)
    set(gca, 'YScale', 'log')
    semilogy(TT,L2)
    xlabel('\tau')
    ylabel('Error')
    figure(4)
    clf
    plot(cos(thetas(1,:)),sin(thetas(1,:)),'k *',cos(th),sin(th))
    hold on
    plot(cos(thetas(end,:)),sin(thetas(end,:)),'b o',cos(th),sin(th))
