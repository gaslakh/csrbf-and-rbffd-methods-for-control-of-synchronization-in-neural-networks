clc
clear;
%%Case 6 control of synchronization of neuron population with
%%Hudgkin-Huxley (HH) PRC (stochastic)
%% Method : RBFFD
N=150;
omega=2*pi/14.638;
T=14.638;
M=200;
S=5*M;
c=80;
dt=T/M;
BB=10;
K=400;
count=0;
b=26;
mu=pi;
population=100;
TT=linspace(0,5*T,S+1);
domain=linspace(0,2*pi,N);
[X,Y]=meshgrid(domain,domain);
load('PRCs.mat')      %HH PRC
phi=zeros(S+1,N);
phif=zeros(S+1,N);
dphi=zeros(S+1,N);
ddphi=zeros(S+1,N);
dphif=zeros(S+1,N);
phi(1,:)=(exp(b*cos(domain-pi))/(2*pi*besseli(0,b)));
phif(1,:)=1/(2*pi);
dphi(1,:)=(-b*sin(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b));
ddphi(1,:)=(-b*cos(domain-pi).*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b))+((b^2*sin(domain-pi).^2.*exp(b*cos(domain-pi)))/(2*pi*besseli(0,b)));
dphif(1,:)=0;
load('RBFFDmatrices.mat')   %%Prefabricated RBF matrices
u=zeros(S+1,1);
L2=zeros(S+1,1);
L2(1)=trapz(domain,(phi(1,:)-phif(1,:)).^2);
D=0.00045;%0.00045 0.0018 0.0040
sqrt(2*D)
STB=((sqrt(2*D)^2/(2*pi))*trapz(domain,(Z.^2)))/2;
th=linspace(0,2*pi,1000);
thetas=zeros(length(S),population);
thetas(1,:)=randraw('vonmises', [pi, b], population ); 
tic;
for i=1:S
    I=(trapz(domain,(phi(i,:)-phif(1,:)).*Zp'.*phi(i,:)));
    G=-STB*(trapz(domain,(dphi(i,:)-dphif(1,:)).*dphi(i,:)));
    A1=w+dt*(omega*wd+u(i)*diag(Zp)*w+u(i)*diag(Z)*wd-STB*wdd);
    B1=phi(i,:);
    A2=w(1,:)-w(N,:);
    B2=0;
    A=[A1;A2];
    B=[B1,B2];
    phi(i+1,:)=A\B';
	dphif(i+1,:)=dphif(i,:);
    L2(i+1)=trapz(domain,(phi(i+1,:)-phif(1,:)).^2);
    dphi(i+1,:)=(wd*phi(i+1,:)');
    ddphi(i+1,:)=(wdd*phi(i+1,:)');
    u(i+1)=min(abs(-K*I-G./I),BB)*I/abs(I); 
     thetas(i+1,:)=RK4StockHH(thetas(i,:),u(i+1),T,dt,D);               %The effect of control parameter on 100 uncoupled neurons
end
    TIME=toc
    figure(1)
    plot(TT,u)
    xlabel('\tau')
    ylabel('u(\tau)')
    figure(2)
    plot(domain,phi(1,:))
    hold on 
    plot(domain,phi(101,:))
    plot(domain,phi(201,:))
    plot(domain,phi(501,:))
    plot(domain,phi(1001,:))
    plot(domain,phif(1,:))
    hold off
    ylim([0 2]);
    xlim([0 2*pi])
    xlabel('\theta')
    ylabel('\rho')
    figure(3)
    set(gca, 'YScale', 'log')
    semilogy(TT,L2)
    xlabel('\tau')
    ylabel('Error')
    figure(4)
    clf
    plot(cos(thetas(1,:)),sin(thetas(1,:)),'k *',cos(th),sin(th))
    hold on
    plot(cos(thetas(end,:)),sin(thetas(end,:)),'r o',cos(th),sin(th))