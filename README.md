# CSRBF and RBFFD methods for control of synchronization in neural networks

MATLAB codes related to the article "Phase distribution control of neural oscillator populations using local radial
basis function meshfree technique with application in epileptic seizures: A
numerical simulation approach"